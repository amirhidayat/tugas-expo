import React, { Component } from 'react';
import {
StyleSheet,
Text,
View,
StatusBar ,
Image,
TouchableOpacity
} from 'react-native';

export default class Login extends Component<{}> {

render() {
    return(
        <View style={styles.container}>
          <View style={styles.header}></View>
          <Image style={styles.logo} source={require('./assets/logo_sanber.png')}/>
          <View style={styles.signupTextCont}>
          <Text style={styles.MidText}>Login</Text>
          </View>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
           
            <View style={styles.formGroup}>
                <Text style={styles.formfieldTitle}>Username / Email</Text>  
                <View style={styles.rectangle}></View>
                <Text style={styles.formfieldTitle}>Password</Text>  
                <View style={styles.rectangle}></View>
            </View>

            <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.buttonLightBlue}>
                <Text style={styles.buttonTitle}>Masuk</Text>  
              </TouchableOpacity>   
              <Text  style={styles.textLightBlue}>atau</Text>             
              <TouchableOpacity style={styles.buttonDarkBlue}>
                <Text style={styles.buttonTitle}>Daftar ?</Text> 
              </TouchableOpacity>
            </View>
         
        
        </View>
        </View>
        </View>
        )
        }
    }  
    
    
    
    const styles = StyleSheet.create({

    header:{
        backgroundColor: "white",
        height:130,
      },
      logo: {
        width: 375,
        height: 116,
         marginBottom:5,
        alignSelf:'center',
        position: 'absolute',
        marginTop:20
      },    
      MidText: {
        color:'#003366',
        fontSize:26,
        fontWeight: 650
        
        },
        body:{
            marginTop:5,
          },
          bodyContent: {
            flex: 1,
            alignItems: 'center',
            padding:10,
          },  
          formGroup: {
            paddingLeft: 15,
            flexDirection: 'column',
            justifyContent: 'space-between',
          },  
          formfieldTitle: {
            color:'#003366',
            fontSize:16
            },                          
    signupTextCont : {
    flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:26,
    flexDirection:'row'
    },
      rectangle: {
        marginTop:5,
        height:40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:15,
        width:350,
        borderWidth: 2,
        borderColor: "#003366",
        backgroundColor: "#FFFFFF",

      },
    buttonTitle: {
    color:'white',
    fontSize:22,
    fontWeight:'200' 
    },
    buttonContainer: {
        paddingTop: 20,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-between',
      },    
      buttonDarkBlue: {
        height:40,
        alignItems: 'center',
        width:150,
        borderRadius:20,
        backgroundColor: "#003366", 
        marginBottom: 15
      },
      textLightBlue: {
        color:'#3EC6FF',
        fontSize:22,
        fontWeight:'200', 
        marginBottom: 15
      }  ,      
      buttonLightBlue: {
        height:40,
        alignItems: 'center',
        width:150,
        borderRadius:20,
        backgroundColor: "#3EC6FF" ,
        marginBottom: 15
      }      
    });