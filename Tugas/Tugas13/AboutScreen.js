import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class Profile extends Component {

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.header}>
          <Text style={styles.pageTitle}>Tentang Saya</Text>
          </View>
          <Image style={styles.avatar} source={{uri: 'http://by.web.id/img/Park-Min-young_128.png'}}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>Amir Aja</Text>
              <Text style={styles.info}>React Native Developer</Text>

            <View style={styles.portoContainer}>
              <Text style={styles.info}>Portofolio</Text>
              <View style={styles.garis} /> 
              <View style={styles.portoContent}>
                <TouchableOpacity style={styles.porto2Content}>
                <MaterialCommunityIcons name="gitlab" size={60} color="#3EC6FF" /> 
                <Text> @amirhidayat </Text>  
                </TouchableOpacity>
                <TouchableOpacity style={styles.porto2Content}> 
                <MaterialCommunityIcons name="github-circle" size={60} color="#3EC6FF" /> 
                <Text> @amirhidayat </Text>  
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.contactContainer}>
              <Text style={styles.info}>Hubungi Saya</Text>
              <View style={styles.garis} /> 
              <View style={styles.contactContent}>
                
                <TouchableOpacity style={styles.contact2Content}>
                <MaterialCommunityIcons name="facebook-box" size={40} color="#3EC6FF" /> 
                <Text> @amirhidayat </Text>  
                </TouchableOpacity>
                
                <TouchableOpacity style={styles.contact2Content}> 
                <MaterialCommunityIcons name="instagram" size={40} color="#3EC6FF" /> 
                <Text> @amirhidayat </Text>  
                </TouchableOpacity>
                <TouchableOpacity style={styles.contact2Content}> 
                <MaterialCommunityIcons name="twitter" size={40} color="#3EC6FF" /> 
                <Text> @amirhidayat </Text>  
                </TouchableOpacity>                
              </View>
            </View>

            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#00BFFF",
    alignItems: 'center',
    height:150,
  },
  pageTitle:{
    marginTop:20,
    fontSize:32,
    color:"#003366",
    fontWeight:'600',
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:80
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  body:{
    marginTop:40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:30,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#00BFFF",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  portoContainer: {
    marginTop:25,
    paddingLeft: 15,
    paddingRight: 15,
    height:140,
    marginBottom:10,
    width:359,
    borderRadius:16,
    backgroundColor: "#EFEFEF",
  },
  garis: {
    marginTop: 5,
    height: 0.7, 
    backgroundColor:'#003366'
  },
  portoContent: {
    flex: 1,
    flexDirection: 'row',    
    alignItems: 'center',
    justifyContent: 'space-around'
  },  
  porto2Content: {
    alignItems: 'center',
    padding:10,
  }, 
  contactContainer: {
    paddingLeft: 15,
    paddingRight: 15,
    height:250,
    marginBottom:10,
    width:359,
    borderRadius:16,
    backgroundColor: "#EFEFEF",
  },  
  contactContent: {
    flex: 1,
    flexDirection: 'column',    
    alignItems: 'center',
    justifyContent: 'space-around'
  },  
  contact2Content: {
    flex: 1,
    flexDirection: 'row',    
    alignItems: 'center',
    padding:10,
  },    
});
 