import React, { Component } from 'react';
import {
StyleSheet,
Text,
View,
StatusBar ,
Image,
TouchableOpacity
} from 'react-native';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Login = ({ route }) => (
  
  <ScreenContainer>
          <View style={styles.header}></View>
          <Image style={styles.logo} source={require('./assets/logo_sanber.png')}/>
         
          <View style={styles.rowCont}>
          <Text style={styles.MidText}>Login</Text>
          </View>
         
          <View style={styles.rowCont}>
          <View style={styles.formGroup}>
                <Text style={styles.formfieldTitle}>Username / Email</Text>  
                <View style={styles.rectangle}></View>
                <Text style={styles.formfieldTitle}>Password</Text>  
                <View style={styles.rectangle}></View>
            </View>

          </View>
          <View style={styles.rowCont}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.buttonLightBlue}>
                <Text style={styles.buttonTitle}>Masuk</Text>  
              </TouchableOpacity>   
              <Text  style={styles.textLightBlue}>atau</Text>             
              <TouchableOpacity style={styles.buttonDarkBlue}>
                <Text style={styles.buttonTitle}>Daftar ?</Text> 
              </TouchableOpacity>
            </View>
         

          </View>
        

        </ScreenContainer>
        );
 
 
    
    const styles = StyleSheet.create({

    header:{
        backgroundColor: "white",
        height:120,
      },
      logo: {
        width: 275,
        height: 86,
         marginBottom:5,
        alignSelf:'center',
        position: 'absolute',
        marginTop:10
      },    
      MidText: {
        color:'#003366',
        fontSize:26,
        fontWeight: "bold",
        marginTop:20
        },
        body:{
      justifyContent :'center',
      flexDirection:'row'
          },
          bodyContent: {
            flex: 1,
            alignItems: 'center',
            padding:10,
          },  
          formGroup: {
            paddingLeft: 15,
            flexDirection: 'column',
            justifyContent: 'space-between',
            marginTop:20
          },  
          formfieldTitle: {
            color:'#003366',
            fontSize:16
            },                          
    rowCont : {
      justifyContent :'center',
      flexDirection:'row'
    },
      rectangle: {
        marginTop:5,
        height:40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:15,
        width:350,
        borderWidth: 2,
        borderColor: "#003366",
        backgroundColor: "#FFFFFF",

      },
    buttonTitle: {
    color:'white',
    fontSize:22,
    fontWeight:'200' 
    },
    buttonContainer: {
        marginTop: 20,
        paddingTop: 20,
        alignItems: 'center',
        flexDirection: 'column',
      },    
      buttonDarkBlue: {
        height:40,
        alignItems: 'center',
        width:150,
        borderRadius:20,
        backgroundColor: "#003366", 
        marginBottom: 15
      },
      textLightBlue: {
        color:'#3EC6FF',
        fontSize:22,
        fontWeight:'200', 
        marginBottom: 15
      }  ,      
      buttonLightBlue: {
        height:40,
        alignItems: 'center',
        width:150,
        borderRadius:20,
        backgroundColor: "#3EC6FF" ,
        marginBottom: 15
      }      
    });