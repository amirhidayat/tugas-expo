import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native'

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );
  
export const Project = () => (

    <ScreenContainer>
        <Text style={styles.textAja}>Halaman Proyek</Text>

      </ScreenContainer>
    );

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textAja: {
    fontSize: 29,
    color: "#003366",
    fontWeight: "bold"
},
})