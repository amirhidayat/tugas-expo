import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { createDrawerNavigator } from "@react-navigation/drawer"
 
import { Login } from "./LoginScreen"    
import { About } from "./AboutScreen"  
import { Skill } from "./SkillScreen"  
import { Project } from "./ProjectScreen"  
import { Tambah } from "./AddScreen"    

const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const TambahStack = createStackNavigator();

const HomeStackScreen = () => (
<HomeStack.Navigator>
  <HomeStack.Screen name="Login" component={Login} />
 
</HomeStack.Navigator>
);

const SkillStackScreen = () => (
    <SkillStack.Navigator>
      <SkillStack.Screen name="Skill" component={Skill} />
    </SkillStack.Navigator>
    );

const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
  );

const TambahStackScreen = () => (
    <TambahStack.Navigator>
      <TambahStack.Screen name="Tambah" component={Tambah} />
    </TambahStack.Navigator>
    );
  

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Skill" component={SkillStackScreen} />
    <Tabs.Screen name="Project" component={ProjectStackScreen} />
    <Tabs.Screen name="Tambah" component={TambahStackScreen} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();

export default () => (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={TabsScreen} />
      <Drawer.Screen name="About" component={AboutStackScreen} />
    </Drawer.Navigator>

 

  </NavigationContainer>

);