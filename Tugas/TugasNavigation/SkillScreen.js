import React, { Component } from 'react';
import {
    FlatList,
    SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import skillList from './skillData.json';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Skill = () => ( 
  <ScreenContainer>
          <Text style={styles.pageTitle}>Home - Skill</Text>
      <View style={styles.containerPutih}>
          <View style={styles.header}>
          <Image source={require('./assets/logo_sanber.png')} style={{width:160, height:52}} />
          <Icon name="account-circle" size={25} />
          <Text style={styles.name}>Amir Aja</Text>
              <Text style={styles.info}>SKILL</Text>
              <View style={styles.garis} /> 

          </View>
          <View style={styles.body}>
            <View style={styles.bodyContent}>

            <View style={styles.buttonTab}>
            <TouchableOpacity style={styles.buttonLightBlue}>
                <Text style={styles.buttonTitle}>Library / Framework</Text>  
              </TouchableOpacity>   
              <TouchableOpacity style={styles.buttonLightBlue}>
                <Text style={styles.buttonTitle}>Bahasa Pemrograman</Text> 
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonLightBlue}>
                <Text style={styles.buttonTitle}>Teknologi</Text> 
              </TouchableOpacity>
            </View>
         

             <FlatList
             data={skillList.items}
             showsVerticalScrollIndicator={false}
             renderItem={renderItem}
             keyExtractor={(item, index) => index.toString()}
           />
    
 
            </View>
        </View>
      </View>
     </ScreenContainer>
    );
 

const renderItem = ({item}) => {
//let item = this.props.item; 
return (

<View style={styles.portoContainer}>
 
  <View style={styles.portoContent}>
 
  <View>
    <MaterialCommunityIcons name={item.iconName} size={80} color="#003366" /> 
    </View>

 
    <TouchableOpacity style={styles.porto2Content}>
    <Text style={styles.info}>{item.skillName}</Text>
    <Text>{item.categoryName}</Text>
    <Text style={styles.persen}> {item.percentageProgress} </Text>  
    </TouchableOpacity>
 
    <TouchableOpacity style={styles.porto2Content}> 
        <MaterialCommunityIcons name="chevron-right" size={90} color="#003366" /> 
        </TouchableOpacity>
 

  </View>

 
 
 
</View>
      
 
)
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#eaeaea"
      },
    containerPutih: {
        padding: 5,
        marginTop: 5,
        margin: 10,
        backgroundColor: "white"
      },
  header:{
    backgroundColor: "white",
    alignItems: 'center',
    //height:180,
  },
  pageTitle:{
    marginTop:5,
    fontSize:20,
    paddingLeft:10,
    color:"#003366",
    fontWeight:'400',
  },
  body:{
    marginTop:40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:30,
  },
  name:{
    fontSize:12,
    color: "#003366",
    fontWeight: "300"
  },
  info:{
    fontSize:26,
    color: "#003366",
    fontWeight: "600"
  },
  persen:{
    fontSize:60,
    color: "white",
    fontWeight: "500"
    
  },  
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  buttonTab: {
    flex: 1,
    flexDirection: 'row', 
    marginBottom: 5,
  },
  buttonTitle: {
    backgroundColor: "#B4E9FF",
    paddingLeft: 5, 
  },  
  portoContainer: {
    flex: 1,
    flexDirection: 'row', 
    marginTop: 2,
    paddingLeft: 2,
    paddingRight: 2,
    height:150,
    marginBottom:10,
    width:343,
    borderRadius: 8,
    backgroundColor: "#B4E9FF",
  },
  garis: {
    marginTop: 5,
    height: 1, 
    backgroundColor:'#003366'
  },
  portoContent: {
    flex: 1,
    flexDirection: 'row',    
    alignItems: 'center',
    justifyContent: 'space-around'
  },  
  porto2Content: {
    alignItems: 'center',
    //padding:10,
  }, 
 
});
 